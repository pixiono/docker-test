FROM docker.io/nginx

LABEL io.openshift.expose-services="8080:http"

USER 0

RUN sed -i "s/listen       80/listen       8080/g" /etc/nginx/conf.d/default.conf && \
    chgrp -R 0 /etc/nginx /var/log/nginx /var/cache/nginx /var/run && \
    chmod g=u /etc/nginx /var/log/nginx /var/cache/nginx /var/run

USER 1001

EXPOSE 8080